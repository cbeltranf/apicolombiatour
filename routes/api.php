<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group([
    'middleware' => 'api',
    'prefix' => 'v1'
], function () {
    Route::apiResource('etapas', 'EtapasController');
    Route::apiResource('camisetas', 'CamisetasController');
    Route::apiResource('corredores', 'CorredoresController');
    Route::apiResource('equipos', 'EquiposController');
    Route::apiResource('noticias', 'NoticiasController');
    Route::apiResource('multimedia', 'MultimediaController');
    Route::get('resultados/general', 'EtapasController@results_general');
    Route::get('resultados/etapa/{id}', 'EtapasController@results_etapa');
    Route::get('resultados/camisetas/{cam}/etapa/{id}', 'CamisetasController@results_etapa');
    Route::get('resultados/corredor/{corr}/general', 'CamisetasController@results_corr_general');

    Route::post('noticias/like/add', 'NoticiasController@like');
    Route::post('noticias/like/remove', 'NoticiasController@dislike');
    Route::post('multimedia/share/add', 'NoticiasController@share');
    Route::get('banner/get', 'EquiposController@banner');
    Route::get('sponsors/get', 'EquiposController@sponsors');
    Route::get('sponsors/section/get', 'MultimediaController@patrocinadores');
    Route::get('about/us', 'MultimediaController@us');
    Route::get('resultados', 'ResultadosController@index');
    Route::post('equipos/members/all', 'EquiposController@team_with_last_general');
});
