<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Equipos extends Model
{
    protected $table = "cyc_equipos";
    protected $primaryKey = 'id';
    protected $guarded = ['id'];
    protected $fillable = ['id','nombre','historia','pais_id','estado','logo', 'camiseta'];


    public function Corredores()
    {
        return $this->hasMany('App\Models\Corredores', 'cyc_equipos_id', 'id');

    }

    public function Pais()
    {
        return $this->belongsTo('App\Models\Paises', 'pais_id', 'id');

    }


    public function scopefilterValue($query, $param)
    {
        $query->orwhere($this->table. ".nombre", 'like', "%$param%");
        $query->orWhere($this->table. ".especialidad", 'like', "%$param%");
    }

    public function getLogoAttribute()
    {
        return 'http://apptourcolombia.com/imagen/equipo/logo/'.$this->attributes['logo'];
    }

    public function getCamisetaAttribute()
    {
        return 'http://apptourcolombia.com/imagen/equipo/camiseta/'.$this->attributes['camiseta'];
    }


}
