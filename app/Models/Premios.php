<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Premios extends Model
{
    protected $table = "cyc_etapa_premio";
    protected $primaryKey = 'id';
    protected $guarded = ['id'];
    protected $dates = ['deleted_at'];
    protected $fillable = ['nombre','detalle','imagen','distancia','cyc_etapas_id','cyc_clase_premios_id','estado'];


    public function Etapa()
    {
        return $this->belongsTo('App\Models\Etapas', 'cyc_etapas_id', 'id');

    }



    /*  CUSTOM SCOPES */


    public function scopefilterValue($query, $param)
    {
        $query->orwhere($this->table. ".nombre", 'like', "%$param%");
        $query->orWhere($this->table. ".detalle", 'like', "%$param%");
        $query->orWhere($this->table. ".distancia", 'like', "%$param%");
    }



}
