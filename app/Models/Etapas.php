<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
class Etapas extends Model
{
    protected $table = "cyc_etapas";
    protected $primaryKey = 'id';
    protected $guarded = ['id'];
    protected $fillable = ['nombre','fecha','longitud','lugar_finaliza','lugar_inicio','ruta','descripcion','estado'];


    public function Premios()
    {
        return $this->hasMany('App\Models\Premios', 'cyc_etapas_id', 'id');

    }


    public function Multimedias()
    {
        return $this->hasMany('App\Models\Multimedia', 'cyc_etapas_id', 'id');

    }



    public function Resultadocamisetaetapa()
    {
        return $this->belongsTo('App\Models\Resultadocamisetaetapa', 'cyc_etapas_id', 'id');

    }

    public function scopefilterValue($query, $param)
    {
        $query->orwhere($this->table. ".nombre", 'like', "%$param%");
        $query->orwhere($this->table. ".descripcion", 'like', "%$param%");
        $query->orWhere($this->table. ".longitud", 'like', "%$param%");
        $query->orWhere($this->table. ".lugar_inicio", 'like', "%$param%");
    }


}
