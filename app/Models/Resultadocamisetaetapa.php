<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
class Resultadocamisetaetapa extends Model
{
    protected $table = "v_resultado_camiseta_etapa";
    protected $primaryKey = 'id';
    protected $guarded = ['id'];
    protected $fillable = ['cyc_camisetas_id', 'cyc_corredores_id', 'cyc_etapas_id', 'acumula', 'tiempo', 'puntos'];

    public function Camisetas()
    {
        return $this->belongsTo('App\Models\Camisetas', 'cyc_camisetas_id', 'id');
    }

    public function Corredores()
    {
        return $this->belongsTo('App\Models\Corredores', 'cyc_corredores_id', 'id');
    }

    public function Etapas()
    {
        return $this->belongsTo('App\Models\Etapas', 'cyc_etapas_id', 'id');
    }


}
