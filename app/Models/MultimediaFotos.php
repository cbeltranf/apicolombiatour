<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MultimediaFotos extends Model
{
    protected $table = "cyc_multimedios_fotos";
    protected $primaryKey = 'id';
    protected $guarded = ['id'];
    protected $fillable = ['nombre', 'cyc_multimedios_id' ];

    public function Multimedia()
    {
        return $this->belongsTo('App\Models\Multimedia', 'cyc_multimedios_id', 'id');

    }


    public function getNombreAttribute()
    {
     return 'http://apptourcolombia.com/imagen/multimediaFoto/'.$this->attributes['nombre'];
    }

}
