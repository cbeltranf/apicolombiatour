<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Jenssegers\Date\Date;

class Multimedia extends Model
{

    protected $table = "cyc_multimedios";
    protected $primaryKey = 'id';
    protected $guarded = ['id'];
    protected $fillable = ['titulo', 'vigencia_desde', 'vigencia_hasta', 'cyc_etapas_id', 'detalle', 'video', 'like', 'reproducciones', 'compartir', 'estado', 'link' ];



    public function scopefilterValue($query, $param)
    {
        $query->orWhere("detalle", 'like', "%$param%");
        $query->orWhere("titulo", 'like', "%$param%");
    }



    public function Etapa()
    {
        return $this->belongsTo('App\Models\Etapas', 'cyc_etapas_id', 'id');

    }

    public function MultimediaFotos()
    {
        return $this->hasMany('App\Models\MultimediaFotos', 'cyc_multimedios_id', 'id');

    }

    public function getVigenciaDesdeAttribute($value)
    {
        Date::setLocale('es');
        return Date::parse($this->attributes['vigencia_desde'])->format('l j F Y');
    }


}
