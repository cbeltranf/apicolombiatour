<?php

namespace App\Models;

use Jenssegers\Date\Date;

use Illuminate\Database\Eloquent\Model;

class Noticias extends Model
{

    protected $table = "cyc_noticias";
    protected $primaryKey = 'id';
    protected $guarded = ['id'];
    protected $fillable = ['titulo', 'vigencia_desde', 'vigencia_hasta', 'detalle_corto', 'detalle_largo', 'foto', 'like', 'reproducciones', 'estado', 'link', 'compartir'];

    public function scopefilterValue($query, $param)
    {
        $query->orWhere("detalle_corto", 'like', "%$param%");
        $query->orWhere("detalle_largo", 'like', "%$param%");
        $query->orWhere("titulo", 'like', "%$param%");
    }


    public function getFotoAttribute()
    {
     return 'http://apptourcolombia.com/imagen/noticia/'.$this->attributes['foto'];
    }

    public function getVigenciaDesdeAttribute($value)
    {
        Date::setLocale('es');
        return Date::parse($this->attributes['vigencia_desde'])->format('l j F Y');
    }


}
