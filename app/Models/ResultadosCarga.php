<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
class ResultadosCarga extends Model
{
    protected $table = "cyc_resultados_manual";
    protected $primaryKey = 'id';
    protected $guarded = ['id'];
    protected $fillable = ['cyc_camiseta_id','cyc_etapa_id','cyc_corredor_id','posicion_etapa','posicion_general','dato_etapa','dato_general','bonificacion_etapa','diferencia_etapa','diferencia_general'];

    public function Camiseta()
    {
        return $this->belongsTo('App\Models\Camisetas', 'cyc_camiseta_id', 'id');

    }

    public function Corredor()
    {
        return $this->belongsTo('App\Models\Corredores', 'cyc_corredor_id', 'id');

    }

    public function Etapa()
    {
        return $this->belongsTo('App\Models\Etapas', 'cyc_etapa_id', 'id');

    }

}
