<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
class Resultadocamiseta extends Model
{
    protected $table = "v_resultados_camiseta";
    protected $primaryKey = 'id';
    protected $guarded = ['id'];
    protected $fillable = ['cyc_camisetas_id', 'cyc_corredores_id', 'cyc_grupos_id', 'acumula', 'tiempo', 'puntos'];

    public function Camisetas()
    {
        return $this->belongsTo('App\Models\Camisetas', 'cyc_camisetas_id', 'id');

    }

    public function Corredores()
    {
        return $this->belongsTo('App\Models\Corredores', 'cyc_corredores_id', 'id');

    }



}
