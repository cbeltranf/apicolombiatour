<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\ResultadosCarga;
use App\Models\Paises;

class Corredores extends Model
{
    protected $table = "cyc_corredores";
    protected $primaryKey = 'id';
    protected $guarded = ['id'];
    protected $fillable = ['id','numero','nombre','especialidad','fecha_nacimiento','pais_id','foto','equipos','palmares','cyc_equipos_id','eslider','estado'];
    protected $appends = [ 'resultado_general_ultima_etapa'];

    public $timestamps = false;


    public function Equipo()
    {
        return $this->belongsTo('App\Models\Equipos', 'cyc_equipos_id', 'id');
    }

    public function Pais()
    {
        return $this->belongsTo('App\Models\Paises', 'pais_id', 'id');

    }

    public function Results()
    {
        return $this->hasMany('App\Models\ResultadosCarga', 'cyc_corredor_id', 'id');
    }


    public function Resultadocamisetaetapa()
    {
        return $this->belongsTo('App\Models\Resultadocamisetaetapa', 'cyc_corredores_id', 'id');

    }

    public function scopefilterValue($query, $param)
    {
        $query->orwhere($this->table. ".numero", 'like', "%$param%");
        $query->orwhere($this->table. ".nombre", 'like', "%$param%");
        $query->orWhere($this->table. ".especialidad", 'like', "%$param%");
    }

    public function scopeOnlyActive($query){
        $query->where($this->table. ".estado", '=', "A");
    }

    public function getFotoAttribute()
    {
        if($this->attributes['foto']==''){
            return 'http://apptourcolombia.com/imagen/corredor/0_I_SinImagen.png';
        }
        return 'http://apptourcolombia.com/imagen/corredor/'.$this->attributes['foto'];
    }


    public function getResultadoGeneralUltimaEtapaAttribute()
    {

        $info = ResultadosCarga::where('cyc_resultados_manual.cyc_corredor_id','=', $this->id )
        ->join('cyc_etapas','cyc_etapas.id', '=', 'cyc_resultados_manual.cyc_etapa_id')
        ->where('cyc_etapas.estado','=', 'C')
        ->select("cyc_resultados_manual.*")
        ->get();
         return  $info;

    }


}
