<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Paises extends Model
{
    protected $table = "cyc_paises";
    protected $primaryKey = 'id';
    protected $guarded = ['id'];
    protected $fillable = ['iso','nombre','name','nicename','iso3','numcode','phonecode'];


    public function Equipos()
    {
        return $this->belongsTo('App\Models\Equipos', 'pais_id', 'id');

    }
    public function Corredores()
    {
        return $this->belongsTo('App\Models\Corredores', 'pais_id', 'id');

    }



}
