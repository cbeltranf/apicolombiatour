<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
class Camisetas extends Model
{
    protected $table = "cyc_camisetas";
    protected $primaryKey = 'id';
    protected $guarded = ['id'];
    protected $fillable = ['id','nombre','descripcion','imagen','cyc_clase_premios_id','cyc_grupo_id','calculo','estado','orden'];


    public function Resultadocamisetaetapa()
    {
        return $this->belongsTo('App\Models\Resultadocamisetaetapa', 'cyc_camisetas_id', 'id');

    }

    public function getImagenAttribute()
    {
        return 'http://apptourcolombia.com/imagen/camiseta/'.$this->attributes['imagen'];
    }


}
