<?php

namespace App\Http\Controllers;

use App\Models\Camisetas;
use App\Models\Resultadocamisetaetapa;
use App\Models\Resultadocamiseta;
use Illuminate\Http\Request;
use App\Http\Resources\GlobalCollection;
use DB;

class CamisetasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $item = Camisetas::orderBy('orden', 'desc');

        if (empty($pageSize)) {
            $pageSize = 10000;
        }

        return new GlobalCollection($item->paginate($pageSize));
    }

    public function results_etapa($cam, $id)
    {
        $item = Resultadocamisetaetapa::with('Corredores')->with('Corredores.Equipo')
        ->with('Corredores.Pais')->where("cyc_etapas_id",$id)
        ->where("cyc_camisetas_id",$cam)->get();

        return response()->json($item);
    }



    public function results_corr_general($corr)
    {

        $item = Resultadocamiseta::with("Camisetas")->where("cyc_corredores_id",$corr)->get();

        return response()->json($item);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Etapas  $etapas
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $item = Camisetas::find($id)->toArray();
        return response()->json($item);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Etapas  $etapa
     * @return \Illuminate\Http\Response
     */
    public function edit(Etapas $etapas)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Etapas  $etapa
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Etapas $etapas)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Etapas  $etapas
     * @return \Illuminate\Http\Response
     */
    public function destroy(Etapas $etapas)
    {
        //
    }
}
