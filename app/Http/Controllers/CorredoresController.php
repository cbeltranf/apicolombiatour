<?php

namespace App\Http\Controllers;

use App\Models\Corredores;
use Illuminate\Http\Request;
use App\Http\Resources\GlobalCollection;
use Carbon\Carbon;
class CorredoresController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $filter = $request->input("filterColumn");
        $filterValue = $request->input("filterValue");
        $pageSize = $request->input("pageSize");
        $sortField = $request->input("sortField");
        $sortOrder = ($request->input("sortOrder") == "asc") ? "asc" : "desc";

        if (empty($sortField)) {
            $sortField = "nombre";
        }

        $item = Corredores::orderBy($sortField, $sortOrder);

        if (empty($filter) || $filter == "*") {

            $item->filterValue($filterValue);

        } else {

            $item->where($filter, 'like', "%$filterValue%");

        }

     //   if (empty($pageSize)) {
            $pageSize = 100000000;
      //  }
        $item->with("Pais");
        $item->with("Equipo");
        $item->with("Equipo.Pais");

        return new GlobalCollection($item->paginate($pageSize));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Corredores  $corredores
     * @return \Illuminate\Http\Response
     */
    public function  show($id)
    {

        $item = Corredores::with('Pais')->with('Equipo')->with('Equipo.Pais')->find($id);
        $item->edad = Carbon::parse($item->fecha_nacimiento)->age;
        $item->save();
        return response()->json($item);
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Corredores  $corredores
     * @return \Illuminate\Http\Response
     */
    public function edit(Corredores $corredores)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Corredores  $corredores
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Corredores $corredores)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Corredores  $corredores
     * @return \Illuminate\Http\Response
     */
    public function destroy(Corredores $corredores)
    {
        //
    }
}
