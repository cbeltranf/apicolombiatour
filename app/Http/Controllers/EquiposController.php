<?php

namespace App\Http\Controllers;

use App\Models\Equipos;
use App\Models\Corredores;
use Illuminate\Http\Request;
use App\Http\Resources\GlobalCollection;
use DB;
class EquiposController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $filter = $request->input("filterColumn");
        $filterValue = $request->input("filterValue");
        $pageSize = $request->input("pageSize");
        $sortField = $request->input("sortField");
        $sortOrder = ($request->input("sortOrder") == "asc") ? "asc" : "desc";

        if (empty($sortField)) {
            $sortField = "nombre";
        }

        $item = Equipos::orderBy($sortField, $sortOrder);
        $item->where("estado", '=', "A");

        if (empty($filter) || $filter == "*") {

            $item->filterValue($filterValue);

        } else {

            $item->where($filter, 'like', "%$filterValue%");

        }

      ///  if (empty($pageSize)) {
            $pageSize = 10000000;
      ////  }

        $item->with("Pais");

        $item2 =   new GlobalCollection($item->paginate($pageSize));
        $values = json_decode(json_encode($item2));

        foreach($values->data as $k => $item){

            $corredor_lider = Corredores::where('cyc_equipos_id','=', $item->id )
            ->where('eslider','=', '1')->first();
            $values->data[$k]->lider = $corredor_lider;

        }


        return response()->json($values);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Equipos  $equipos
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $item = Equipos::with('Pais')->find($id)->toArray();
        return response()->json($item);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Equipos  $equipos
     * @return \Illuminate\Http\Response
     */
    public function edit(Equipos $equipos)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Equipos  $equipos
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Equipos $equipos)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\City  $city
     * @return \Illuminate\Http\Response
     */
    public function destroy(City $city)
    {
        //
    }

    public function team_with_last_general(Request $request){
        $data = $request->validate([
            "id" => "required|exists:cyc_equipos"
        ]);

        $item = Corredores::where("cyc_corredores.cyc_equipos_id", "=", $data["id"]);
        $item->where("cyc_corredores.estado", '=', 'A');
        $item->with("Pais");
        $item = $item->get();

        return response()->json($item);
    }

    public function banner(Request $request){
        $item = DB::table("cyc_banners as b")
        ->select(DB::raw(" CONCAT('http://apptourcolombia.com/imagen/banner/', b.imagen) as image, b.nombre, b.id") )
        ->join("cyc_banner_carrusel as c", "c.cyc_banners_id", "=", "b.id" )
        ->where("b.estado", "=", "A")->get();


        return response()->json($item);

    }

    public function sponsors(Request $request){
        $item = DB::table("cyc_patrocinadores as b")
        ->select(DB::raw(" CONCAT('http://apptourcolombia.com/imagen/patrocinadores/', b.logo) as image, b.nombre, b.id, b.enlace, b.orden") )
        ->orderBy('b.orden', 'asc')
        ->where("b.estado", "=", "A")->get();
        return response()->json($item);

    }

}
