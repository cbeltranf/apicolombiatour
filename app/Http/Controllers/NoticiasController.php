<?php

namespace App\Http\Controllers;

use App\Models\Noticias;
use App\Models\Multimedia;
use Illuminate\Http\Request;
use App\Http\Resources\GlobalCollection;
use Carbon\Carbon;
use Jenssegers\Date\Date;

class NoticiasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $filter = $request->input("filterColumn");
        $filterValue = $request->input("filterValue");
        $pageSize = $request->input("pageSize");
        $sortField = $request->input("sortField");
        $sortOrder = ($request->input("sortOrder") == "asc") ? "asc" : "desc";

        if (empty($sortField)) {
            $sortField = "titulo";
        }

        $item = Noticias::orderBy('vigencia_desde', 'desc');

        $item->whereRaw(" cyc_noticias.estado = 'A'  AND ( (     ((cyc_noticias.vigencia_desde IS NOT NULL AND NOW() >= cyc_noticias.vigencia_desde ) OR cyc_noticias.vigencia_desde IS NULL )
        AND (
              (cyc_noticias.vigencia_hasta IS NOT NULL AND NOW() < DATE(cyc_noticias.vigencia_hasta)  )
            OR cyc_noticias.vigencia_hasta IS NULL )      ) )");


        if (empty($filter) || $filter == "*") {

           // $item->filterValue($filterValue);

        } else {

       //     $item->where($filter, 'like', "%$filterValue%");

        }

       // if (empty($pageSize)) {
            $pageSize = 1000000000;
       // }
       $item->select('id','titulo', 'vigencia_desde', 'vigencia_hasta', 'detalle_corto', 'detalle_largo', 'foto', 'like', 'reproducciones', 'estado', 'link', 'compartir');

        return new GlobalCollection($item->paginate($pageSize));


    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $new  = Noticias::find($id);
        $new->reproducciones = $new->reproducciones + 1;
        $new->save();

       $item = Noticias::find($id);

       Date::setLocale('es');
      $date = Date::createFromFormat('l d F Y', $item->vigencia_desde)->format('Y-m-d');
      // dd($date);
      $item->vigencia_desde = $date;
        //$item->vigencia_desde = Date::createFromFormat('l j F Y', $item->vigencia_desde, 'America/Bogota');
       //$item->vigencia_desde = $item->vigencia_desde->format('Y-m-d');
       //   $date->setTimezone('UTC');
      $data = json_decode($item);
      $data->vigencia_desde = $date;
       return response()->json($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function like(Request $request){

        $data = $request->validate([
            "item_id" => "required|integer",
            "tipo" => "required|in:noticia,multimedia",
        ]);


        if($data["tipo"] == "noticia"){

            $new  = Noticias::find($data["item_id"]);
            $new->like = $new->like + 1;
            $new->save();

            }else{

            $new  = Multimedia::find($data["item_id"]);
            $new->like = $new->like + 1;
            $new->save();

            }


            $item["response"]= 'Liked';
            return response()->json($item);
    }


    public function dislike(Request $request){

        $data = $request->validate([
            "item_id" => "required|integer",
            "tipo" => "required|in:noticia,multimedia",
        ]);

        if($data["tipo"] == "noticia"){

        $new  = Noticias::find($data["item_id"]);
        $new->like = $new->like - 1;
        $new->save();

        }else{

        $new  = Multimedia::find($data["item_id"]);
        $new->like = $new->like - 1;
        $new->save();

        }

        $item["response"]= 'Disliked';
        return response()->json($item);
    }


    public function share(Request $request){

        $data = $request->validate([
            "item_id" => "required|integer"
        ]);

            $new  = Multimedia::find($data["item_id"]);
            $new->compartir = $new->compartir + 1;
            $new->save();

            $item["response"]= 'shared';
            return response()->json($item);
    }
}
