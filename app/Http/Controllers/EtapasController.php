<?php

namespace App\Http\Controllers;

use App\Models\Etapas;
use Illuminate\Http\Request;
use App\Http\Resources\GlobalCollection;
use DB;

class EtapasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $filterValue = $request->input("filterValue");
        $pageSize = $request->input("pageSize");

        $item = Etapas::orderBy("nombre", "asc");
        $item->where("nombre", 'like', "%$filterValue%");

       /// if (empty($pageSize)) {
            $pageSize = 100000000;
        ///}

        return new GlobalCollection($item->paginate($pageSize));
    }

    public function results_etapa($id)
    {
        $etapas =  DB::select(
            'call p_resultados_corredor('.$id.',0)'
        );
        return response()->json($etapas);
    }

    public function results_general()
    {
        $etapas =  DB::select(
            'call p_resultados_corredor(0,0)'
        );
        return response()->json($etapas);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Etapas  $etapas
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $etapas = Etapas::with('Premios')->find($id)->toArray();
        return response()->json($etapas);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Etapas  $etapa
     * @return \Illuminate\Http\Response
     */
    public function edit(Etapas $etapas)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Etapas  $etapa
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Etapas $etapas)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Etapas  $etapas
     * @return \Illuminate\Http\Response
     */
    public function destroy(Etapas $etapas)
    {
        //
    }
}
