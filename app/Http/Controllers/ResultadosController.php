<?php

namespace App\Http\Controllers;

use App\Models\ResultadosCarga;
use Illuminate\Http\Request;
use App\Http\Resources\GlobalCollection;
use DB;

class ResultadosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $filter = $request->input("filterColumn");
        $camiseta = $request->input("Camiseta");
        $filterValue = $request->input("filterValue");
        $pageSize = $request->input("pageSize");
        $sortField = $request->input("sortField");
        $sortOrder = ($request->input("sortOrder") == "asc") ? "asc" : "desc";



        if($camiseta == 4){

            $item = ResultadosCarga::where("cyc_camiseta_id", '=', 1);

        }else{
            $item = ResultadosCarga::where("cyc_camiseta_id", '=', $camiseta);
        }


        if ( ( $sortField == "posicion_general" && $camiseta == 4 ) ) {
            $item->select( "id", "cyc_camiseta_id", "cyc_etapa_id", "cyc_corredor_id", DB::raw("posicion_etapa as posicion_general"),  DB::raw("dato_etapa as dato_general"), "bonificacion_etapa",  DB::raw( "CONCAT(diferencia_etapa, ' ', bonificacion_etapa) as diferencia_general") );
            $item->whereNotNull('dato_etapa');
            $sortField = "posicion_etapa" ;
        }else if ( $sortField == "posicion_general"  ||  ( $sortField != "posicion_general" &&  $camiseta != 1 && $camiseta != 4 )) {
            $item->select( "id", "cyc_camiseta_id", "cyc_etapa_id", "cyc_corredor_id", "posicion_general", "dato_general", "bonificacion_etapa",  "diferencia_general");
            $item->whereNotNull('dato_general');
            $sortField = "posicion_general" ;
        }else{
            $item->select( "id", "cyc_camiseta_id", "cyc_etapa_id", "cyc_corredor_id", DB::raw("posicion_etapa as posicion_general"),  DB::raw("dato_etapa as dato_general"), "bonificacion_etapa",  DB::raw( "CONCAT(diferencia_etapa, ' ', bonificacion_etapa) as diferencia_general") );
            $item->whereNotNull('dato_etapa');
            $sortField = "posicion_etapa" ;
        }

        $item->orderBy($sortField, $sortOrder);


        if (empty($filter) || $filter == "*") {

           // $item->filterValue($filterValue);

        } else if(in_array($filter, array("cyc_etapas_id", "cyc_corredor_id"))){

            $item->where($filter, '=', "$filterValue");

        }else{
            $item->where($filter, 'like', "%$filterValue%");
        }



       // if (empty($pageSize)) {
            $pageSize = 10000000;
     //   }

        $item->with('Camiseta');
        $item->with('Corredor');
        $item->with('Corredor.Pais');
        $item->with('Corredor.Equipo');
        $item->with('Etapa');

        return new GlobalCollection($item->paginate($pageSize));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ResultadosCarga  $resultadosCarga
     * @return \Illuminate\Http\Response
     */
    public function show(ResultadosCarga $resultadosCarga)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ResultadosCarga  $resultadosCarga
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ResultadosCarga $resultadosCarga)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ResultadosCarga  $resultadosCarga
     * @return \Illuminate\Http\Response
     */
    public function destroy(ResultadosCarga $resultadosCarga)
    {
        //
    }
}
