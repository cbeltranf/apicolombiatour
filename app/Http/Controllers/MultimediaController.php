<?php

namespace App\Http\Controllers;

use App\Models\Multimedia;
use Illuminate\Http\Request;
use App\Http\Resources\GlobalCollection;
use DB;

class MultimediaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $filter = $request->input("filterColumn");
        $filterValue = $request->input("filterValue");
        $pageSize = $request->input("pageSize");
        $sortField = $request->input("sortField");
        $sortOrder = ($request->input("sortOrder") == "asc") ? "asc" : "desc";

        if (empty($sortField)) {
            $sortField = "titulo";
        }

        $item = Multimedia::orderBy('id', 'desc');
        $item->where('estado', '=', "A");

        if (empty($filter) || $filter == "*") {

            $item->filterValue($filterValue);

        } else if($filter == "cyc_etapas_id"){

            $item->where($filter, '=', "$filterValue");

        }else{


            $item->where($filter, 'like', "%$filterValue%");

        }

       //// if (empty($pageSize)) {
            $pageSize = 1000000;
     ///   }

        $item->with('MultimediaFotos');
        $item->with('Etapa');

        return new GlobalCollection($item->paginate($pageSize));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $new  = Multimedia::find($id);
        $new->reproducciones = $new->reproducciones + 1;
        $new->save();

       $item = Multimedia::find($id);
       $item->MultimediaFotos;
       $item->Etapa;
       return response()->json($item);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function us(){

    }

    public function patrocinadores(){

        $patrocionadores = DB::table("cyc_patrocinadores")->orderBy("orden", "asc")->get();

        return $patrocionadores;

    }
}
